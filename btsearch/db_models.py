import datetime

import sqlalchemy
from sqlalchemy import Column, DateTime, Integer, TypeDecorator, Unicode, UnicodeText
from sqlalchemy.ext.declarative import declarative_base

from btsearch import config
from btsearch.utils import localtime

Base = declarative_base()


class DateTimeInDefaultTimezone(TypeDecorator):
    """
    DateTime field with timezone support for DBs without timezone support.
    All dates are converted to UTC before saving and converted in the project's
    default timezone on load.
    """

    impl = DateTime

    cache_ok = True

    def process_bind_param(self, value, dialect):
        return value.astimezone(datetime.timezone.utc).replace(tzinfo=None)

    def process_result_value(self, value, dialect):
        return value.replace(tzinfo=datetime.timezone.utc).astimezone(config.TIMEZONE)


class ContactMessage(Base):
    __tablename__ = "contactmessage"

    id = Column(Integer, primary_key=True)
    email = Column(Unicode(250))
    body = Column(UnicodeText, nullable=False)
    created_at = Column(DateTimeInDefaultTimezone, nullable=False, default=localtime)


def create_session_maker() -> sqlalchemy.orm.sessionmaker:
    db_url = config.DATABASE_URL
    engine = sqlalchemy.create_engine(db_url, connect_args={"check_same_thread": False})
    return sqlalchemy.orm.sessionmaker(bind=engine)
