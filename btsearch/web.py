import datetime
import html
import json
import logging
import pickle
import re
import secrets
import urllib
from pathlib import Path
from typing import Any, List, Optional, Type, Union, cast

import anyio
import lxml
import lxml.etree
import redis
import uvicorn
from fastapi import Depends, FastAPI, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from lxml.builder import E
from pydantic import BaseModel, validator
from sqlalchemy.orm import sessionmaker
from sse_starlette.sse import EventSourceResponse, ServerSentEvent
from starlette.concurrency import iterate_in_threadpool
from starlette.requests import Request
from starlette.responses import FileResponse, Response

from btsearch.config import (
    ADMIN_PASSWORD,
    ALLOWED_ENGINES,
    ALLOWED_ORIGINS,
    CACHE_TTL,
    DEBUG,
    REDIS_CONF,
)
from btsearch.db_models import ContactMessage, create_session_maker
from btsearch.engines import ENGINES, Engine
from btsearch.helpers import anySizeToBytes, monkeypatch_urllib_force_ipv4
from btsearch.utils import localtime

logger = logging.getLogger(__name__)


ICONS_PATH = Path(__file__).parent / "engines"


# TODO: pass redis as dependency
redis_client = (
    redis.StrictRedis(
        host=REDIS_CONF["HOST"],
        port=REDIS_CONF["PORT"],
        db=REDIS_CONF["DB"],
        password=REDIS_CONF["PASSWORD"],
    )
    if REDIS_CONF
    else None
)

api = FastAPI()
api.add_middleware(
    CORSMiddleware,
    allow_origins=ALLOWED_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


security = HTTPBasic(realm="btsearch admin")


def require_auth(credentials: HTTPBasicCredentials = Depends(security)):
    is_correct_username = secrets.compare_digest(credentials.username, "admin")
    is_correct_password = secrets.compare_digest(credentials.password, ADMIN_PASSWORD)
    if not (is_correct_username and is_correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Basic"},
        )


def db_session_maker(request: Request) -> sessionmaker:
    if not hasattr(request.app.state, "db_session_maker"):
        request.app.state.db_session_maker = create_session_maker()
    return cast(sessionmaker, request.app.state.db_session_maker)


@api.get("/")
async def root():
    return {"description": "API for btsearch"}


def get_available_engines() -> dict[str, Type[Engine]]:
    return {
        engine_id: engine
        for engine_id, engine in ENGINES.items()
        if ALLOWED_ENGINES is None or engine_id in ALLOWED_ENGINES
    }


class EngineData(BaseModel):
    id: str
    name: str
    url: str
    search_url: str
    icon: str


@api.get("/engines", response_model=List[EngineData])
async def engine_list():
    return [
        EngineData(
            id=engine_id,
            name=engine.name,
            url=engine.url,
            search_url=engine.search_url,
            icon=f"/engines/{engine_id}/icon",
        )
        for engine_id, engine in get_available_engines().items()
    ]


@api.get(
    "/engines/{engine_id}/icon",
    responses={
        200: {
            "content": {"image/png": {}, "application/json": None},
        },
    },
)
async def engine_icon(engine_id: str):
    if engine_id not in get_available_engines():
        raise HTTPException(404, detail="Engine not found")

    icon_path = ICONS_PATH.joinpath(f"{engine_id}.png")
    return FileResponse(path=icon_path, media_type="image/png")


def cache_set(key, value) -> bool:
    if not redis_client:
        return False
    try:
        with redis_client.pipeline() as pipe:
            data = pickle.dumps(value)
            pipe.set(key, data)
            pipe.expire(key, CACHE_TTL)
            pipe.execute()
            return True
    except redis.ConnectionError:
        return False


def cache_get(key):
    if not redis_client:
        return None
    value = None
    try:
        data = redis_client.get(key)
        if data:
            value = pickle.loads(data)
    except redis.ConnectionError:
        pass
    return value


class SearchResult(BaseModel):
    source: str
    engine_id: str
    name: str
    size: int
    seeds: int
    leech: int
    epoch: Optional[str]
    download_url: str
    download_info: Optional[str]


def parse_result(result_dict: dict, engine_id: str) -> SearchResult:
    # TODO: Generate SearchResults from engine classes
    return SearchResult(
        source=result_dict["desc_link"],
        engine_id=engine_id,
        name=result_dict["name"],
        size=anySizeToBytes(result_dict["size"]),
        seeds=int(result_dict["seeds"]),
        leech=int(result_dict["leech"]),
        epoch=str(result_dict.get("epoch")) if result_dict.get("epoch") else None,
        download_url=result_dict["link"],
        download_info=result_dict.get("download_info") or None,
    )


def encode_event_data(result: Any) -> str:
    return json.dumps(jsonable_encoder(result))


@api.get(
    "/search/{engine_id}/{keywords}",
    responses={
        200: {
            # TODO: list of results
            "content": {"text/event-stream": {}, "application/json": None},
        },
    },
)
async def search(engine_id: str, keywords: str):
    if engine_id not in get_available_engines():
        raise HTTPException(404, detail="Engine not found")

    async def results_generator():
        cache_key = f"search_results|{engine_id}|{keywords}"
        results = await anyio.to_thread.run_sync(cache_get, cache_key)
        if results is not None:
            for result in results.values():
                yield ServerSentEvent(encode_event_data(result))
            yield ServerSentEvent(event="success")
        else:
            results = {}
            # TODO: encode keywords in engine class
            query = urllib.parse.quote(keywords)
            engine_class = ENGINES[engine_id]
            engine = engine_class()
            try:
                async for result_raw in iterate_in_threadpool(engine.search(query)):
                    result = parse_result(result_raw, engine_id=engine_id)
                    if result.source not in results:
                        results[result.source] = result
                        yield ServerSentEvent(encode_event_data(result))
            except Exception:
                # TODO: negative cache ?
                logger.exception("Error when fetching results.")
                yield ServerSentEvent(
                    encode_event_data("Unexpected error when fetching results"),
                    event="resulterror",
                )
            else:
                await anyio.to_thread.run_sync(cache_set, cache_key, results)
                yield ServerSentEvent(event="success")

    return EventSourceResponse(results_generator())


class NoTorrentDownloaded:
    """
    Used to differentiate between "No result in cache" and "No torrent with this ID".
    """

    pass


def get_cached_torrent(engine: Engine, engine_id: str, info: str) -> Optional[bytes]:
    cache_key = f"torrent_file|{engine_id}|{info}"
    cache_value: Union[Type[NoTorrentDownloaded], None, bytes] = cache_get(cache_key)
    if cache_value is None:
        torrent_content = engine.download_torrent(info)
        cache_value = (
            NoTorrentDownloaded if torrent_content is None else torrent_content
        )
        # TODO: use longer TTL for cached torrents
        cache_set(cache_key, cache_value)
    else:
        logger.debug("Use value from cache for %s", (engine_id, info))

    if cache_value == NoTorrentDownloaded:
        return None
    else:
        return cast(Optional[bytes], cache_value)


@api.get(
    "/download/{engine_id}/{info}",
    responses={
        200: {
            "content": {"application/x-bittorrent": {}, "application/json": None},
        },
        404: {
            "description": "Torrent or engine not found",
        },
    },
)
def torrent_download(engine_id: str, info: str):
    engines = get_available_engines()
    try:
        engine_class = engines[engine_id]
        engine = engine_class()
    except KeyError:
        raise HTTPException(404, detail="Engine not found")

    torrent_content = get_cached_torrent(engine, engine_id, info)
    if not torrent_content:
        raise HTTPException(404, detail="Torrent not found")

    if torrent_content.startswith(b"magnet:"):
        magnet_link = torrent_content.decode()
        return RedirectResponse(url=magnet_link, status_code=302)

    cleaned_info = re.sub(r"[^a-z0-9-_.]", "", info.lower().replace(" ", "_"))
    filename = f"{cleaned_info}.torrent"
    return Response(
        content=torrent_content,
        media_type="application/x-bittorrent",
        headers={
            "content-disposition": f"attachment; filename={filename}",
        },
    )


class ContactMessageIn(BaseModel):
    email: Optional[str]
    body: str

    @validator("email")
    def clean_empty_email(cls, value):
        return value or None

    @validator("body")
    def validate_non_empty_body(cls, value):
        if len(value) == 0:
            raise ValueError("Must be a non-empty string")
        return value


class ContactMessageOut(ContactMessageIn):
    id: str
    created_at: datetime.datetime


@api.post("/contact/messages")
def create_contact_message(
    message: ContactMessageIn,
    db_session_maker: sessionmaker = Depends(db_session_maker),
):
    with db_session_maker.begin() as db:  # type: ignore
        db_message = ContactMessage(email=message.email, body=message.body)
        db.add(db_message)
    return Response(status_code=201)


@api.get(
    "/contact/messages",
    response_model=List[ContactMessageOut],
    responses={
        401: {"description": "Unauthorized"},
    },
)
def contact_message_list(
    db_session_maker: sessionmaker = Depends(db_session_maker),
    require_auth: HTTPBasicCredentials = Depends(require_auth),
):
    with db_session_maker.begin() as db:  # type: ignore
        messages = db.query(ContactMessage).order_by(ContactMessage.created_at.desc())

    return [
        ContactMessageOut(
            id=m.id,
            body=m.body,
            created_at=m.created_at,
            email=m.email,
        )
        for m in messages
    ]


@api.get(
    "/contact/messages.atom",
    responses={
        200: {
            "content": {"application/atom+xml": {}, "application/json": None},
        },
        401: {"description": "Unauthorized"},
    },
)
def contact_message_list_atom(
    db_session_maker: sessionmaker = Depends(db_session_maker),
    require_auth: HTTPBasicCredentials = Depends(require_auth),
):
    with db_session_maker.begin() as db:  # type: ignore
        messages = db.query(ContactMessage).order_by(ContactMessage.created_at.desc())

    latest_message = messages.first()
    updated_date = latest_message.created_at if latest_message else localtime()

    def make_entry(message: ContactMessage):
        title = message.body.replace("\n", " ")
        if len(title) > 90:
            title = title[:87] + "..."

        body = html.escape(message.body)
        body = "<br>".join(body.split("\n"))

        entry = E.entry(
            E.id(str(message.id)),
            E.title(title),
            E.author(E.name(message.email or "-")),
            E.updated(message.created_at.isoformat()),
            E.content(body, type="html"),
        )

        return entry

    doc = E.feed(
        E.title("Contact messages"),
        E.updated(updated_date.isoformat()),
        E.id("urn:uuid:facef10d-a4c9-480c-9882-2926e3699074"),
        *(make_entry(message) for message in messages),
        xmlns="http://www.w3.org/2005/Atom",
    )

    return Response(
        content=lxml.etree.tostring(doc, pretty_print=True),
        media_type="application/atom+xml",
    )


app = FastAPI()
app.mount("/api", api, name="api")


def main():
    uvicorn_kwargs = dict(host="0.0.0.0", port=8080)
    # TODO: fix hidden logging.debug in engine modules
    if DEBUG:
        logging.basicConfig(level=logging.DEBUG)
        uvicorn_kwargs["reload"] = True
    else:
        logging.basicConfig(level=logging.INFO)

    # Ensure that we use the same source IP as Flaresolverr
    monkeypatch_urllib_force_ipv4()

    uvicorn.run("btsearch.web:app", **uvicorn_kwargs)


if __name__ == "__main__":
    main()
