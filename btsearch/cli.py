# VERSION: 1.41

# Author:
#  Fabien Devaux <fab AT gnux DOT info>
# Contributors:
#  Christophe Dumez <chris@qbittorrent.org> (qbittorrent integration)
#  Thanks to gab #gcu @ irc.freenode.net (multipage support on PirateBay)
#  Thanks to Elias <gekko04@users.sourceforge.net> (torrentreactor and isohunt search engines)
#
# Licence: BSD

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright notice,
#      this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the author nor the names of its contributors may be
#      used to endorse or promote products derived from this software without
#      specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import json
import logging
import sys
import urllib.parse
from typing import Type

from .config import DEBUG
from .engines import ENGINES, Engine
from .helpers import monkeypatch_urllib_force_ipv4, prettyPrinter

################################################################################
# Every engine should have a "search" method taking
# a space-free string as parameter (ex. "family+guy")
# it should call prettyPrinter() with a dict as parameter.
# The keys in the dict must be: link,name,size,seeds,leech,engine_url
# As a convention, try to list results by decreasing number of seeds or similar
################################################################################


def serialize_engine(engine_id: str, engine: Type[Engine]) -> dict:
    return {
        "id": engine_id,
        "class": engine.__name__,
        "name": engine.name,
        "url": engine.url,
        "search_url": engine.search_url,
    }


def display_engines(engines: dict[str, Type[Engine]]):
    """
    Display engines in JSON format
    """
    serialized_engines = [
        serialize_engine(engine_id, engine) for engine_id, engine in engines.items()
    ]
    print(json.dumps(serialized_engines, indent=2))


def run_search(engine_class: Type[Engine], query: str):
    """Run search in engine

    @retval False if any exceptions occurred
    @retval True  otherwise
    """
    try:
        engine = engine_class()
        results = engine.search(query)
        for result in results:
            prettyPrinter(result)
        return True
    except Exception:
        if DEBUG:
            raise
        return False


def main():
    if DEBUG:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    # Ensure that we use the same source IP as Flaresolverr
    monkeypatch_urllib_force_ipv4()

    args = sys.argv[1:]
    if not args:
        raise SystemExit(
            "btsearch_cli [all|engine1[,engine2]*] <keywords>\n"
            "available engines: %s" % (",".join(ENGINES))
        )
    elif args[0] in ("--capabilities", "--engines"):
        display_engines(ENGINES)
        sys.exit()
    elif len(args) < 2:
        raise SystemExit(
            "btsearch_cli [all|engine1[,engine2]*] <keywords>\n"
            "available engines: %s" % (",".join(ENGINES))
        )

    # get only unique engines with set
    engines_list = set(e.lower() for e in args[0].strip().split(","))

    if "all" in engines_list:
        engines_list = list(ENGINES.keys())
    else:
        # discard un-supported engines
        engines_list = [engine for engine in engines_list if engine in ENGINES]

    if not engines_list:
        # engine list is empty. Nothing to do here
        sys.exit()

    query = urllib.parse.quote(" ".join(args[1:]))
    success = all(run_search(ENGINES[engine], query) for engine in engines_list)
    status_code = int(not success)
    sys.exit(status_code)


if __name__ == "__main__":
    main()
