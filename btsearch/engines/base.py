from abc import abstractmethod
from typing import Iterable, Optional, Protocol


class Engine(Protocol):
    @property
    @abstractmethod
    def name(self) -> str:
        """Pretty name"""

    @property
    @abstractmethod
    def url(self) -> str:
        """Absolute URL of home page"""

    @property
    @abstractmethod
    def search_url(self) -> str:
        """
        Absolute URL of the first page of search results.
        Must contain `%s` as a placeholder for the search query.
        """

    @abstractmethod
    def search(self, query: str) -> Iterable[dict]:
        ...

    def download_torrent(self, info: str) -> Optional[bytes]:
        """
        Download and return torrent file identified with `info`.
        Return `None` if no torrent with `info` can be downloaded.
        """
        pass
