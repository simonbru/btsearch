from ..helpers import tor_session
from .piratebay import PiratebayEngine


class PiratebayOnionEngine(PiratebayEngine):
    url = "http://piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion.pet"
    backend_url = (
        "http://piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion"
    )
    name = "The Pirate Bay (via Tor)"

    def get_session(self):
        return tor_session()
