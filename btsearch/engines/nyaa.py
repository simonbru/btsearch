import re

import bs4
import requests

from .base import Engine


class NyaaEngine(Engine):
    url = "https://nyaa.si"
    search_url = url + "/?f=0&c=0_0&s=seeders&o=desc&q=%s"
    name = "Nyaa"

    def __init__(self):
        self.session = requests.Session()

    def parse_results(self, soup):
        rows = soup.select(".torrent-list tbody tr")
        for row in rows:
            result = {"engine_url": self.url}
            cells = row.find_all("td")
            for link in cells[1].find_all("a"):
                match = re.match("^/view/(\d+)$", link.get("href", ""))
                if match:
                    result["desc_link"] = self.url + link["href"]
                    result["name"] = link["title"]
                    torrent_id = match.group(1)
                    result["link"] = f"{self.url}/download/{torrent_id}.torrent"
                    break
            else:
                raise ValueError("Could not find link to torrent page")
            result["size"] = cells[3].text.strip()
            result["epoch"] = cells[4]["data-timestamp"].strip()
            seeders = cells[5].text
            result["seeds"] = int(seeders.strip())
            leechers = cells[6].text
            result["leech"] = int(leechers.strip())
            yield result

    def find_next_page_url(self, soup):
        link = soup.select_one(".next a")
        if link and link.get("href"):
            return self.url + link.get("href")

    def search(self, what):
        search_url = self.search_url % what
        for _page in range(7):
            data = self.session.get(search_url).text
            soup = bs4.BeautifulSoup(data, "html5lib")
            yield from self.parse_results(soup)
            search_url = self.find_next_page_url(soup)
            if not search_url:
                break
