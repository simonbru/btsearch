# VERSION: 2.0
# AUTHORS: Douman (custparasite@gmx.se)
# CONTRIBUTORS: Diego de las Heras (ngosang@hotmail.es)


from html.parser import HTMLParser

import dateparser

from ..helpers import retrieve_url
from .base import Engine


class ResultsParser(HTMLParser):
    """Sub-class for parsing results"""

    def __init__(self, url):
        HTMLParser.__init__(self)
        self.results = []
        self.url = url
        self.article_found = False  # true when <article> with results is found
        self.item_found = False
        self.item_bad = False  # set to True for malicious links
        self.current_item = None  # dict for found item
        self.item_name = None  # key's name in current_item dict
        self.parser_class = {
            "td": "human_date",
            "ts": "size",
            "tul": "seeds",
            "tdl": "leech",
        }

    def handle_starttag(self, tag, attrs):
        params = dict(attrs)
        if self.item_found:
            if tag == "td":
                if "class" in params:
                    self.item_name = self.parser_class.get(params["class"], None)
                    if self.item_name:
                        self.current_item[self.item_name] = ""

        elif self.article_found and tag == "a":
            if "href" in params:
                link = params["href"]
                if link.startswith("/torrent"):
                    self.current_item["desc_link"] = "".join((self.url, link))
                    self.current_item["link"] = "".join(
                        (self.url, "/tor/", link.split("/")[2], ".torrent")
                    )
                    self.current_item["engine_url"] = self.url
                    self.item_found = True
                    self.item_name = "name"
                    self.current_item["name"] = ""
                    self.item_bad = "rel" in params and params["rel"] == "nofollow"

        elif tag == "article":
            self.article_found = True
            self.current_item = {}

    def handle_data(self, data):
        if self.item_name:
            self.current_item[self.item_name] += data

    def handle_endtag(self, tag):
        if tag == "article":
            self.article_found = False
        elif self.item_name and (tag == "a" or tag == "td"):
            self.item_name = None
        elif self.item_found and tag == "tr":
            self.item_found = False
            if not self.item_bad:
                human_date = self.current_item.pop("human_date", None)
                if human_date:
                    date = dateparser.parse(
                        human_date,
                        locales=["en"],
                        settings={
                            "TIMEZONE": "UTC",
                            "RETURN_AS_TIMEZONE_AWARE": True,
                        },
                    )
                    self.current_item["epoch"] = str(int(date.timestamp()))
                self.current_item["seeds"] = int(self.current_item["seeds"])
                self.current_item["leech"] = int(self.current_item["leech"])
                self.results.append(self.current_item)
            self.current_item = {}


class TorlockEngine(Engine):
    url = "https://www.torlock2.com"
    _search_url = url + "/all/torrents/{query}.html?sort=seeds&order=desc&page={page}"
    search_url = _search_url.format(query="%s", page=1)
    name = "TorLock"

    def search(self, query):
        query = query.replace("%20", "-")
        parser = ResultsParser(self.url)
        for i in range(1, 6):
            page_url = self._search_url.format(query=query, page=i)
            html = retrieve_url(page_url)
            parser.feed(html)
            if not parser.results:
                break
            yield from parser.results
            parser.results.clear()
        parser.close()
