import base64
import binascii
import logging
import re
from typing import cast

import bs4
import dateparser

from btsearch.engines.base import Engine
from btsearch.helpers import CloudflareBypassSession

logger = logging.getLogger(__name__)


class T1337xEngine(Engine):
    url = "https://1337x.to"
    search_url = f"{url}/search/%s/1/"
    name = "1337x"

    def __init__(self):
        self.session = CloudflareBypassSession(self.__class__.__name__)

    def download_torrent(self, info: str) -> bytes | None:
        try:
            decoded_info = base64.urlsafe_b64decode(info).decode()
        except (ValueError, binascii.Error, UnicodeDecodeError):
            logger.debug("Invalid download info: %s", info, exc_info=True)
            return None

        if not re.match(r"[0-9]+/[^?]+", decoded_info):
            logger.debug("Invalid download info: %s", decoded_info)
            return None

        page_url = f"{self.url}/torrent/{decoded_info}"
        data = self.session.request_text("GET", page_url)
        soup = bs4.BeautifulSoup(data, "html5lib")
        magnet_link = soup.select_one('.torrent-detail-page a[href^="magnet:"]')
        if not magnet_link:
            logger.debug("Could not find magnet link in page: %s", page_url)
            return None

        magnet_link_uri = cast(str, magnet_link["href"])
        return magnet_link_uri.encode()

    def parse_results(self, soup):
        rows = soup.select(".search-page tbody tr")
        for row in rows:
            result = {"engine_url": self.url}
            link = row.select_one('.name a[href^="/torrent/"]')
            url_path = link["href"]
            result["desc_link"] = self.url + url_path
            result["name"] = link.text.strip()
            result["link"] = ""
            dl_info = url_path.split("/torrent/", maxsplit=1)[1]
            dl_info_b64 = base64.urlsafe_b64encode(dl_info.encode()).decode()
            result["download_info"] = dl_info_b64

            seeds_cell = row.select_one(".seeds")
            result["seeds"] = int(seeds_cell.text.strip())

            leech_cell = row.select_one(".leeches")
            result["leech"] = int(leech_cell.text.strip())

            size_cell = row.select_one(".size")
            # Ignore the hidden unrelated span in this cell
            result["size"] = list(size_cell.children)[0].text.strip()

            date_cell = row.select_one(".coll-date")
            date = dateparser.parse(
                date_cell.text,
                locales=["en"],
                settings={
                    "TIMEZONE": "UTC+1",
                    "RETURN_AS_TIMEZONE_AWARE": True,
                },
            )
            result["epoch"] = str(int(date.timestamp()))
            yield result

    def find_next_page_url(self, soup):
        last_active = False
        for page_link in soup.select(".pagination li a"):
            if "active" in page_link.parent.get("class", []):
                last_active = True
                continue

            if last_active:
                url_path = page_link["href"].lstrip("/")
                return f"{self.url}/{url_path}"

    def search(self, what):
        search_url = self.search_url % what
        for _page in range(5):
            data = self.session.request_text("GET", search_url)
            soup = bs4.BeautifulSoup(data, "html5lib")
            yield from self.parse_results(soup)
            search_url = self.find_next_page_url(soup)
            if not search_url:
                break
