import logging
import re
from urllib.parse import urljoin, urlparse

import bs4
from requests import Response

from btsearch.config import RUTRACKER_LOGIN, RUTRACKER_PASS
from btsearch.helpers import persistent_session

from .base import Engine

logger = logging.getLogger(__name__)

# Query params:
# f[] = -1  => categories
# o = 10    => sort by seeder
# s = 2     => descending order
# pn        => ??
# nm        => title contains

CREDENTIALS = {
    "login_username": RUTRACKER_LOGIN,
    "login_password": RUTRACKER_PASS,
}


class LoginError(Exception):
    pass


class RutrackerEngine(Engine):
    url = "https://rutracker.org"
    search_url = url + "/forum/tracker.php?o=10&s=2&nm=%s"
    name = "RuTracker (RU)"

    login_path = "/forum/login.php"
    forum_url = url + "/forum/"

    def __init__(self):
        self.session = persistent_session(self.__class__.__name__)

    def _download_url(self, topic_id):
        return self.forum_url + f"dl.php?t={topic_id}"

    def download_torrent(self, info):
        if not info.isdigit():
            logger.debug("Invalid download info: %s", info)
            return

        url = self._download_url(info)
        response = self.request(url)
        response.raise_for_status()

        content_type = response.headers.get("content-type", "")
        if "application/x-bittorrent" not in content_type:
            logger.debug("Tracker did not return a torrent for: %s", info)
            return
        return response.content

    def parse_results(self, soup):
        tbody = soup.find(id="tor-tbl").select_one("tbody")
        if tbody.text.strip() == "Не найдено":  # This means "Not found"
            return
        rows = tbody.select("tr")
        for row in rows:
            result = {
                "engine_url": self.url,
            }
            link = row.select_one("a.tLink")
            result["name"] = link.text.strip()
            result["desc_link"] = urljoin(self.forum_url, link["href"])
            topic_id = re.search(r"t=(\d+)", link["href"]).group(1)
            result["download_info"] = topic_id
            result["link"] = self._download_url(topic_id)
            result["size"] = row.select_one(".tor-size").attrs["data-ts_text"].strip()
            cells = row.find_all("td")
            seeds = int(cells[6].attrs["data-ts_text"].strip())
            # A negative number means "number of days since there is no more seeders"
            result["seeds"] = max(0, seeds)
            result["leech"] = int(cells[7].text.strip())
            result["epoch"] = cells[-1].attrs["data-ts_text"].strip()
            yield result

    def find_next_page_url(self, soup):
        next_page_link = soup.select_one(".bottom_info").find("a", string="След.")
        if next_page_link and "href" in next_page_link.attrs:
            return urljoin(self.forum_url, next_page_link["href"])

    def search(self, what):
        search_url = self.search_url % what
        for page in range(6):
            data = self.request(search_url).text
            soup = bs4.BeautifulSoup(data, "html5lib")
            yield from self.parse_results(soup)
            search_url = self.find_next_page_url(soup)
            if not search_url:
                break

    def request(self, url) -> Response:
        res: Response = self.session.get(url)
        res_url = urlparse(res.url)
        if res.history and res_url.path.startswith(self.login_path):
            # Redirected to login page. Try to login.
            if not all(CREDENTIALS.values()):
                raise LoginError("RUTRACKER_LOGIN and RUTRACKER_PASS must be set.")
            next_url = res_url.query.split("redirect=")[1]
            login_res: Response = self.session.post(
                self.url + self.login_path,
                data={
                    **CREDENTIALS,
                    "login": "Вход",
                    "redirect": next_url,
                },
            )
            if not urlparse(login_res.url).path.startswith(self.login_path):
                return login_res
            else:
                raise LoginError("Failed to login on rutracker.")
        else:
            return res
