from typing import Type

from . import nyaa, piratebay, piratebayo, rutracker, t1337x, torlock, ygg
from .base import Engine

ENGINES: dict[str, Type[Engine]] = {
    "nyaa": nyaa.NyaaEngine,
    "piratebay": piratebay.PiratebayEngine,
    "piratebayo": piratebayo.PiratebayOnionEngine,
    "rutracker": rutracker.RutrackerEngine,
    "1337x": t1337x.T1337xEngine,
    "torlock": torlock.TorlockEngine,
    "ygg": ygg.YggEngine,
}
