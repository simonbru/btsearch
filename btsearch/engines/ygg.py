import re
from urllib.parse import urlparse

import bs4

from btsearch.config import YGG_DOMAIN, YGG_LOGIN, YGG_PASS
from btsearch.engines.base import Engine
from btsearch.helpers import CloudflareBypassSession

CREDENTIALS = {
    "id": YGG_LOGIN,
    "pass": YGG_PASS,
}


class LoginError(Exception):
    pass


class YggEngine(Engine):
    domain = YGG_DOMAIN
    url = "https://{}".format(domain)
    search_url = url + "/engine/search?name=%s&do=search&order=desc&sort=seed"
    name = "YGGTorrent (FR)"

    login_page_path = "/auth/login"
    login_process_path = "/auth/process_login"

    session: CloudflareBypassSession

    def __init__(self):
        self.session = CloudflareBypassSession(self.__class__.__name__)

    def parse_results(self, soup):
        if soup.find("h2", string=re.compile("aucun résultat", re.IGNORECASE)):
            return
        rows = soup.find(id="#torrents").select("table tbody tr")
        for row in rows:
            result = {
                "engine_url": self.url,
            }
            cells = row.find_all("td")
            for link in cells[1].find_all("a"):
                result["name"] = link.text.strip()

                raw_url = link.get("href", "")
                result["desc_link"] = raw_url
                url = urlparse(raw_url)
                torrent_id = url.path.split("/")[-1].split("-")[0]
                result["link"] = "{}/engine/download_torrent?id={}".format(
                    self.url, torrent_id
                )
            result["epoch"] = cells[4].find("div").text.strip()
            result["size"] = cells[5].text.strip()
            seeders = cells[7].text
            result["seeds"] = int(seeders.strip())
            leechers = cells[8].text
            result["leech"] = int(leechers.strip())
            yield result

    def find_next_page_url(self, soup):
        link = soup.find("a", rel="next")
        if link:
            return link.get("href")

    def search(self, what):
        path = "/engine/search?name={}&do=search&order=desc&sort=seed".format(what)
        search_url = self.url + path
        for page in range(7):
            data = self.request(search_url)
            soup = bs4.BeautifulSoup(data, "html5lib")
            yield from self.parse_results(soup)
            search_url = self.find_next_page_url(soup)
            if not search_url:
                break

    def request(self, url) -> str:
        res = self.session.request("get", url)
        res_url = urlparse(res.url)
        if res_url.path.startswith(self.login_page_path):
            # Redirected to login page. Try to login.
            if not all(CREDENTIALS.values()):
                raise LoginError("YGG_LOGIN and YGG_PASS must be set.")
            login_res = self.session.request(
                "post",
                self.url + self.login_process_path,
                data=CREDENTIALS,
                headers={
                    "X-Requested-With": "XMLHttpRequest",
                },
            )
            if "Bad credentials" in login_res.text:
                raise LoginError("Failed to login on rutracker.")

            final_res = self.session.request("get", url)
            return final_res.text
        else:
            return res.text
