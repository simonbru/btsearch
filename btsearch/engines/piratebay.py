import html

from ..helpers import generate_magnet_link, persistent_session
from .base import Engine


class PiratebayEngine(Engine):
    _search_path = "q.php?cat=0&q=%s"

    url = "https://thepiratebay.org"
    search_url = f"{url}/search.php?q=%s"
    backend_url = "https://apibay.org"
    # backend_url = (
    #     "http://piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion"
    # )
    name = "The Pirate Bay"

    def __init__(self):
        self.session = self.get_session()

    def get_session(self):
        # return tor_session()
        return persistent_session("piratebay")

    def search(self, what):
        # Note: The pirate bay has no pagination (yet) since its rewrite
        # "what" is already urlencoded
        page_url = f"{self.backend_url}/{self._search_path}" % what
        data = self.session.get(page_url).json()
        for item in data:
            if item["id"] == "0":
                # Fake result returned when there are no results
                return
            yield {
                "engine_url": self.url,
                # The '&' char is html-escaped for some reason
                "name": html.unescape(item["name"]),
                "seeds": int(item["seeders"]),
                "leech": int(item["leechers"]),
                "desc_link": f"{self.url}/description.php?id={item['id']}",
                "link": generate_magnet_link(
                    info_hash=item["info_hash"], name=item["name"]
                ),
                "size": item["size"],
                "epoch": item["added"],
            }
