# VERSION: 1.40

# Author:
#  Christophe DUMEZ (chris@qbittorrent.org)

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright notice,
#      this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the author nor the names of its contributors may be
#      used to endorse or promote products derived from this software without
#      specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import gzip
import html.entities
import io
import json
import logging
import re
import socket
import urllib.error
import urllib.parse
import urllib.request
from dataclasses import dataclass
from http.cookiejar import MozillaCookieJar
from pathlib import Path

import requests
import urllib3
from decouple import config
from requests.cookies import create_cookie

from btsearch.config import ENGINE_STORAGE, FLARESOLVERR_URL

logger = logging.getLogger(__name__)

PUBLIC_TRACKERS = [
    "udp://tracker.coppersurfer.tk:6969/announce",
    "udp://tracker.opentrackr.org:1337",
    "udp://tracker.zer0day.to:1337/announce",
    "udp://tracker.leechers-paradise.org:6969/announce",
    "udp://9.rarbg.me:2850/announce",
    "udp://9.rarbg.to:2920/announce",
    "udp://explodie.org:6969/announce",
]

FLARESOLVERR_TIMEOUT_MS = 20_000

# Some sites blocks default python User-agent
DEFAULT_HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:125.0) Gecko/20100101 Firefox/125.0"
}


def htmlentitydecode(s):
    # First convert alpha entities (such as &eacute;)
    # (Inspired from http://mail.python.org/pipermail/python-list/2007-June/443813.html)
    def entity2char(m):
        entity = m.group(1)
        if entity in html.entities.name2codepoint:
            return chr(html.entities.name2codepoint[entity])
        return " "  # Unknown entity: We replace with a space.

    t = re.sub("&(%s);" % "|".join(html.entities.name2codepoint), entity2char, s)

    # Then convert numerical entities (such as &#233;)
    t = re.sub("&#(\d+);", lambda x: chr(int(x.group(1))), t)

    # Then convert hexa entities (such as &#x00E9;)
    return re.sub("&#x(\w+);", lambda x: chr(int(x.group(1), 16)), t)


def retrieve_url(url):
    """Return the content of the url page as a string"""
    req = urllib.request.Request(url, headers=DEFAULT_HEADERS)
    try:
        response = urllib.request.urlopen(req)
    except urllib.error.URLError as errno:
        print(" ".join(("Connection error:", str(errno.reason))))
        return ""
    dat = response.read()
    # Check if it is gzipped
    if dat[:2] == b"\x1f\x8b":
        # Data is gzip encoded, decode it
        compressedstream = io.BytesIO(dat)
        gzipper = gzip.GzipFile(fileobj=compressedstream)
        extracted_data = gzipper.read()
        dat = extracted_data
    info = response.info()
    charset = "utf-8"
    try:
        ignore, charset = info["Content-Type"].split("charset=")
    except Exception:
        pass
    dat = dat.decode(charset, "replace")
    dat = htmlentitydecode(dat)
    # return dat.encode('utf-8', 'replace')
    return dat


def generate_magnet_link(info_hash, name):
    params = {
        "xt": f"urn:btih:{info_hash}",
        "dn": name,
        "tr": PUBLIC_TRACKERS,
    }
    encoded_params = urllib.parse.urlencode(params, doseq=True, safe=":")
    return f"magnet:?{encoded_params}"


def storage_folder(name):
    fpath = Path(ENGINE_STORAGE) / name
    fpath.mkdir(parents=True, exist_ok=True)
    return fpath


def monkeypatch_urllib_force_ipv4():
    def monkeypatched_allowed_gai_family():
        return socket.AF_INET

    urllib3.util.connection.allowed_gai_family = monkeypatched_allowed_gai_family


def persistent_session(name, *args, **kwargs):
    """
    Return `requests` session with persistent cookies and
    automatic handling of Cloudflare challenges.
    :param name: session name (to keep state separate between engines)
    """
    folder = storage_folder(name)
    scraper = requests.Session(*args, **kwargs)

    cookie_path = folder / "cookiejar"
    scraper.cookies = MozillaCookieJar(cookie_path.as_posix())
    if cookie_path.exists():
        scraper.cookies.load()

    storage_path = folder / "browser_state.json"
    state = {}
    if storage_path.exists():
        state = json.loads(storage_path.read_text())
        scraper.headers["User-Agent"] = state["user-agent"]

    def save_state_hook(*args, **kwargs):
        scraper.cookies.save()
        state["user-agent"] = scraper.headers["User-Agent"]
        storage_path.write_text(json.dumps(state))

    scraper.hooks["response"] = save_state_hook

    return scraper


class FlaresolverrError(Exception):
    pass


@dataclass(frozen=True)
class LightResponse:
    status: int
    url: str
    text: str


class CloudflareBypassSession:
    """
    HTTP session with persistent cookies and
    automatic handling of Cloudflare challenges via Flaresolverr.
    :param name: session name (to keep state separate between engines)
    """

    scraper: requests.Session
    state: dict
    storage_path: str

    def __init__(self, name, *args, **kwargs):
        folder = storage_folder(name)
        self.scraper = requests.Session(*args, **kwargs)

        # Use same user-agent and cookies (in particular "cf_clearance") as flaresolverr
        # Note that the source IP must also be the same, which is always an IPv4 for flaresolverr

        cookie_path = folder / "cookiejar"
        self.scraper.cookies = MozillaCookieJar(cookie_path.as_posix())
        if cookie_path.exists():
            self.scraper.cookies.load()

        self.storage_path = folder / "browser_state.json"
        self.state = {}
        if self.storage_path.exists():
            self.state = json.loads(self.storage_path.read_text())
            self.scraper.headers["User-Agent"] = self.state["user-agent"]

        self.scraper.hooks["response"] = lambda *args, **kwargs: self._save_state()

    def _save_state(self):
        self.scraper.cookies.save()
        self.state["user-agent"] = self.scraper.headers["User-Agent"]
        self.storage_path.write_text(json.dumps(self.state))

    def request(self, method, url, *args, data=None, **kwargs) -> LightResponse:
        if method.lower() not in ("get", "post"):
            raise ValueError("Flaresolverr only supports methods GET and POST")

        kwargs = kwargs | {"allow_redirects": True}
        response = self.scraper.request(method, url, *args, **kwargs)
        if not (
            response.headers.get("Server", "").startswith("cloudflare")
            and (response.status_code == 403 or response.text == "")
        ):
            return LightResponse(
                status=response.status_code,
                url=response.url,
                text=response.text,
            )

        logger.debug(
            "Request blocked by cloudflare. Retry through flaresolverr: %s", url
        )
        body = {
            "cmd": f"request.{method.lower()}",
            "url": url,
            # TODO: why is timeout reached for some requests ?
            "maxTimeout": FLARESOLVERR_TIMEOUT_MS,
        }
        if method.lower() == "post":
            post_data = urllib.parse.urlencode(data)
            body["postData"] = post_data
        response = requests.post(f"{FLARESOLVERR_URL}/v1", json=body)
        # logger.debug(
        #     "Flaresolverr response for %s and status %s: %s",
        #     url,
        #     response.status_code,
        #     response.text,
        # )
        if not response.status_code == 200:
            raise FlaresolverrError(
                f"Unexpected HTTP response code {response.status_code}"
            )
        response_data = response.json()
        if not response_data.get("status") == "ok":
            raise FlaresolverrError(
                f"Unexpected in-body status: {response_data.get('status')}"
            )

        self.scraper.headers["User-Agent"] = response_data["solution"]["userAgent"]
        for cookie_data in response_data["solution"]["cookies"]:
            expires = cookie_data.get("expiry")
            cookie = create_cookie(
                name=cookie_data["name"],
                value=cookie_data["value"],
                domain=cookie_data["domain"] or "",
                path=cookie_data["path"],
                secure=cookie_data["secure"],
                expires=expires,
                discard=expires is None,
            )
            self.scraper.cookies.set_cookie(cookie)
        self._save_state()

        solution = response_data["solution"]
        return LightResponse(
            status=solution["status"], url=solution["url"], text=solution["response"]
        )

    def request_text(self, method, url, *args, data=None, **kwargs) -> str:
        return self.request(method, url, *args, data=data, **kwargs).text


def tor_session():
    session = requests.Session()
    tor_socks: str = config("TOR_SOCKS_URL", default="socks5h://localhost:9050")
    if tor_socks:
        session.proxies = {
            "http": tor_socks,
            "https": tor_socks,
        }
    return session


def prettyPrinter(dictionary):
    dictionary["size"] = anySizeToBytes(dictionary["size"])
    outtext = "|".join(
        (
            dictionary["link"],
            dictionary["name"].replace("|", " "),
            str(dictionary["size"]),
            str(dictionary["seeds"]),
            str(dictionary["leech"]),
            dictionary["engine_url"],
            dictionary.get("desc_link", ""),
            str(dictionary.get("epoch") or ""),
            str(dictionary.get("download_info") or ""),
        )
    )

    # fd 1 is stdout
    with open(1, "w", encoding="utf-8", closefd=False) as utf8stdout:
        print(outtext, file=utf8stdout)


def anySizeToBytes(size_string, thousand_sep=","):
    """
    Convert a string like '1 KB' to '1024' (bytes)
    """
    # separate integer from unit
    try:
        size, unit = size_string.split()
    except Exception:
        try:
            size = size_string.strip()
            unit = "".join([c for c in size if c.isalpha()])
            if len(unit) > 0:
                size = size[: -len(unit)]
        except Exception:
            return -1
    if len(size) == 0:
        return -1
    size = size.replace(thousand_sep, "")
    size = float(size)
    if len(unit) == 0:
        return int(size)
    short_unit = unit.upper()[0]

    # convert
    units_dict = {"T": 40, "G": 30, "M": 20, "K": 10}
    if short_unit in units_dict:
        size = size * 2 ** units_dict[short_unit]
    return int(size)
