import datetime

from btsearch import config


def localtime():
    return datetime.datetime.now(tz=config.TIMEZONE)
