import os
from datetime import timedelta
from zoneinfo import ZoneInfo

import decouple
import dj_redis_url

DEBUG = decouple.config("DEBUG", cast=bool, default=False)

ALLOWED_ENGINES: list[str] | None = (
    decouple.config("ALLOWED_ENGINES", cast=decouple.Csv(), default="") or None
)

ADMIN_PASSWORD = decouple.config(
    "ADMIN_PASSWORD", default="admin" if DEBUG else decouple.undefined
)

TIMEZONE = ZoneInfo("Europe/Zurich")

DATABASE_URL = decouple.config(
    "DATABASE_URL", default="sqlite:///data/db.sqlite" if DEBUG else decouple.undefined
)

FLARESOLVERR_URL = decouple.config(
    "FLARESOLVERR_URL", default="http://localhost:8191" if DEBUG else decouple.undefined
)

REDIS_CONF: dict | None = (
    decouple.config("REDIS_URL", cast=dj_redis_url.parse)
    if os.environ.get("REDIS_URL")
    else None
)

ENGINE_STORAGE: str = decouple.config(
    "ENGINE_STORAGE", default="/tmp/nova2_engine_storage"
)

CACHE_TTL: int = decouple.config(
    "CACHE_TTL",
    default=timedelta(minutes=30).total_seconds(),
    cast=int,
)


if DEBUG:
    ALLOWED_ORIGINS = ["*"]
else:
    ALLOWED_ORIGINS = []


RUTRACKER_LOGIN: str = decouple.config("RUTRACKER_LOGIN", default="")
RUTRACKER_PASS: str = decouple.config("RUTRACKER_PASS", default="")

YGG_DOMAIN: str = decouple.config("YGG_DOMAIN", default="www.ygg.re")
YGG_LOGIN: str = decouple.config("YGG_LOGIN", default="")
YGG_PASS: str = decouple.config("YGG_PASS", default="")
