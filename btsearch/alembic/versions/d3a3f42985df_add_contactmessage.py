"""Add contactmessage

Revision ID: d3a3f42985df
Revises: 
Create Date: 2022-01-07 23:23:29.020573

"""

import sqlalchemy as sa
from alembic import op

import btsearch

# revision identifiers, used by Alembic.
revision = "d3a3f42985df"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "contactmessage",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("email", sa.Unicode(length=250), nullable=True),
        sa.Column("body", sa.UnicodeText(), nullable=False),
        sa.Column(
            "created_at", btsearch.db_models.DateTimeInDefaultTimezone(), nullable=False
        ),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("contactmessage")
