import { useEffect, useState } from "react";
import { Alert, Button, Col, Form, Row, Spinner } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { getContactUrl, setDocumentTitle } from "../utils";

export default function ContactPage() {
  useEffect(() => {
    setDocumentTitle("Contact");
  }, []);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [status, setStatus] = useState("idle");
  const isSubmitting = status === "loading";

  const onSubmit = async (data) => {
    setStatus("loading");

    try {
      const jsonData = JSON.stringify(data);
      const response = await fetch(getContactUrl(), {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: jsonData,
      });
      if (!response.ok) {
        throw new Error(`Response: ${response.status}`);
      }
      setStatus("success");
    } catch (error) {
      console.error(error);
      setStatus("error");
    }
  };

  return (
    <Row>
      <Col md="7" xl="6">
        <h2 className="fs-4 mb-4">Contact</h2>

        {status === "success" ? (
          <Alert variant="success">Thank you for the feedback!</Alert>
        ) : (
          <>
            <p className="mb-4">
              Use this form to raise an issue, give a suggestion, provide
              feedback, etc.
            </p>

            <Form onSubmit={handleSubmit(onSubmit)}>
              <Form.Group className="mb-3">
                <Form.Label>
                  Your email address
                  <span className="text-muted"> (optional)</span>
                </Form.Label>

                <Form.Control
                  type="email"
                  placeholder="my@email.com"
                  isInvalid={errors.email}
                  {...register("email", { maxLength: 150 })}
                />
                {errors.email && (
                  <Form.Control.Feedback type="invalid">
                    Email address is too long.
                  </Form.Control.Feedback>
                )}
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Message</Form.Label>
                <Form.Control
                  as="textarea"
                  placeholder="e.g. I would love to see this or that tracker integrated."
                  rows="8"
                  isInvalid={errors.body}
                  {...register("body", { required: true })}
                />
                {errors.body && (
                  <Form.Control.Feedback type="invalid">
                    This field is required.
                  </Form.Control.Feedback>
                )}
              </Form.Group>

              {status === "error" && (
                <Alert variant="danger">
                  An error occurred when sending your message.
                </Alert>
              )}

              <Button variant="primary" type="submit" disabled={isSubmitting}>
                Submit
              </Button>
              {isSubmitting && (
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                  variant="secondary"
                  className="ms-2"
                />
              )}
            </Form>
          </>
        )}
      </Col>
    </Row>
  );
}
