# Changelog

## 2024-06-23
- YGGTorrent: Fix broken cloudflare bypass

## 2024-06-09
- YGGTorrent: Fix for combination of cloudflare + required login

## 2024-05-25
- YGGTorrent: Add support for required login
- 1337x: Fix empty results

## 2024-03-16
- 1337x: Fix (bypass Cloudflare bot protection)

## 2023-08-03
- 1337x: Add engine

## 2023-03-09
- Torlock: Use alternate domain www.torlock2.com because the canonical one has weird TLS issues.

## 2022-01-30
- Add contact form

## 2021-12-08
- Fix overlapping text with long search queries

## 2021-12-04
- Use more subtle colors for search engine checkboxes

## 2021-11-28
- Add page for changelog

## 2021-11-26
- Change the style of search engine checkboxes
- Show result count and loading state for each search engine
