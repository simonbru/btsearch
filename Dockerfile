FROM python:3.11-bullseye as builder

ENV APP_PATH=/app \
    VIRTUAL_ENV=/opt/venv \
    PATH="/opt/venv/bin:$PATH"

WORKDIR $APP_PATH

ENV PYTHONUNBUFFERED=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    # Custom vars
    PIP_VERSION=22.3.1 \
    POETRY_VERSION=1.2.2

# Install pip and poetry
RUN set -eux; \
    python3 -m ensurepip \
    && pip3 install --upgrade pip==${PIP_VERSION} poetry==${POETRY_VERSION}

# Install python dependencies
RUN python -m venv "$VIRTUAL_ENV" \
    && "$VIRTUAL_ENV"/bin/python -m pip install pip==${PIP_VERSION}
COPY pyproject.toml poetry.lock ./
RUN poetry install --only main --no-root

# Install app
COPY btsearch/ ./btsearch/
COPY alembic.ini ./
RUN poetry install --only main


# Production image
FROM python:3.11-slim-bullseye

ENV APP_PATH=/app \
    VIRTUAL_ENV=/opt/venv \
    PATH="/opt/venv/bin:$PATH"

WORKDIR $APP_PATH

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
        nodejs  # needed to bypass cloudflares bot check \
        curl  # just for easy debugging \
	; \
	rm -rf /var/lib/apt/lists/*

COPY --from=builder "$VIRTUAL_ENV" "$VIRTUAL_ENV"
COPY --from=builder "$APP_PATH" "$APP_PATH"

EXPOSE 8080

CMD ["python", "-m", "btsearch.web"]
