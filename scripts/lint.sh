#!/usr/bin/env bash
set -x

mypy btsearch tests
flake8 btsearch tests
