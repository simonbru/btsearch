#!/usr/bin/env bash
set -x

python -m pytest ./tests "${@}"
