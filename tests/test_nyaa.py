from btsearch.engines.nyaa import NyaaEngine

ITEMS_PER_PAGE = 75


def test_first_page_result():
    results = NyaaEngine().search(r"attaque%20des%20titans")
    expected_name = "Shingeki no Kyojin (L’Attaque des Titans) Saison 3 – 11 VOSTFR"
    for result in results:
        if result["name"] == expected_name:
            break
    else:
        raise AssertionError(f"Missing expected result: {expected_name}")

    assert result["link"] == "https://nyaa.si/download/1082088.torrent"
    assert result["size"] == "153.5 MiB"
    assert result["desc_link"] == "https://nyaa.si/view/1082088"
    assert result["epoch"] == "1538939831"
    assert type(result["seeds"]) == int
    assert type(result["leech"]) == int


def test_multiple_pages():
    results = list(NyaaEngine().search(r"attack%20titan%20x264"))
    assert len(results) > ITEMS_PER_PAGE
