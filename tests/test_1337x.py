from btsearch.engines.t1337x import T1337xEngine


def test_second_page_result():
    results = list(T1337xEngine().search(r"atlanta%20hevc"))
    expected_name = "Atlanta (2016) - S02E06 (1080p AMZN WEB-DL x265 HEVC 10bit EAC3 6.0 RZeroX) [QxR]"
    for result in results:
        if result["name"] == expected_name:
            break
    else:
        raise AssertionError(f"Missing expected result: {expected_name}")

    assert result["link"] == ""
    assert (
        result["desc_link"]
        == "https://1337x.to/torrent/2911771/Atlanta-2016-S02E06-1080p-AMZN-WEB-DL-x265-HEVC-10bit-EAC3-6-0-RZeroX-QxR/"
    )
    assert (
        result["download_info"]
        == "MjkxMTc3MS9BdGxhbnRhLTIwMTYtUzAyRTA2LTEwODBwLUFNWk4tV0VCLURMLXgyNjUtSEVWQy0xMGJpdC1FQUMzLTYtMC1SWmVyb1gtUXhSLw=="
    )
    assert result["epoch"] == "1523142000"
    assert result["size"] == "1.4 GB"
    assert type(result["seeds"]) == int
    assert type(result["leech"]) == int


def test_no_result():
    results = list(T1337xEngine().search(r"ubuntu%2006.04"))
    assert results == []
