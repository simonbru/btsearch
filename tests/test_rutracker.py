from btsearch.engines.rutracker import RutrackerEngine


def test_first_page_result():
    results = RutrackerEngine().search(r"attack%20titans")
    expected_name = (
        "Attack on Titan: Wings of Freedom [P] [ENG / MULTI3 / JPN] (2016) (+ 4 DLC)"
    )
    for result in results:
        if result["name"] == expected_name:
            break
    else:
        raise AssertionError(f"Missing expected result: {expected_name}")

    assert result["link"] == "https://rutracker.org/forum/dl.php?t=5274154"
    assert result["size"] == "7380626375"
    assert result["desc_link"] == "https://rutracker.org/forum/viewtopic.php?t=5274154"
    assert result["epoch"] == "1472315491"
    assert type(result["seeds"]) == int
    assert type(result["leech"]) == int


def test_no_result():
    results = list(RutrackerEngine().search(r"ubuntu%2006.04"))
    assert results == []
