import re

from btsearch.engines.ygg import YggEngine


def test_first_page_result():
    results = YggEngine().search(r"avengers%20earth")
    expected_name = (
        "Avengers Earth's Mightiest Heroes.INTEGRALE.TRUEFRENCH.720p.BluRay.x264-FTMVHD"
    )
    for result in results:
        if result["name"] == expected_name:
            break
    else:
        raise AssertionError(f"Missing expected result: {expected_name}")

    assert (
        result["link"] == "https://www2.yggtorrent.se/engine/download_torrent?id=691336"
    )
    assert result["size"] == "21.89Go"
    assert re.match(
        r"https://\w+.yggtorrent.\w+/torrent/filmvidéo/animation-série/691336-avengers\+earths\+mightiest\+heroes\+integrale\+truefrench\+720p\+bluray\+x264-ftmvhd",
        result["desc_link"],
    )
    assert result["epoch"] == "1606326705"
    assert type(result["seeds"]) == int
    assert type(result["leech"]) == int


def test_no_result():
    results = list(YggEngine().search(r"ubuntu%2012.04"))
    assert results == []
