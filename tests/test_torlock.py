from btsearch.engines.torlock import TorlockEngine


def test_first_page_result():
    results = TorlockEngine().search(r"tame%20impala")
    expected_name = "Tame Impala - Currents (Deluxe) (2015) [FLAC]"
    for result in results:
        if result["name"] == expected_name:
            break
    else:
        raise AssertionError(f"Missing expected result: {expected_name}")

    assert result["link"] == "https://www.torlock.com/tor/33935031.torrent"
    assert result["size"] == "550 MB"
    assert (
        result["desc_link"]
        == r"https://www.torlock.com/torrent/33935031/tame-impala-currents-%28deluxe%29-%28%29-%5Bflac%5D.html"
    )
    assert result["epoch"] == "1608854400"
    assert type(result["seeds"]) == int
    assert type(result["leech"]) == int


def test_no_result():
    results = list(TorlockEngine().search(r"ubuntu%2006.04"))
    assert results == []
