from btsearch.engines.piratebay import PiratebayEngine


def test_first_page_result():
    results = PiratebayEngine().search(r"attack%20titan")
    expected_name = "Attack on Titan Season 2 Shingeki no Kyojin Dual Audio 720p"
    for result in results:
        if result["name"] == expected_name:
            break
    else:
        raise AssertionError(f"Missing expected result: {expected_name}")

    assert (
        result["link"]
        == "magnet:?xt=urn:btih:17ED929C58FCE05D7A7A90259B38D46543376746&dn=Attack+on+Titan+Season+2+Shingeki+no+Kyojin+Dual+Audio+720p&tr=udp:%2F%2Ftracker.coppersurfer.tk:6969%2Fannounce&tr=udp:%2F%2Ftracker.opentrackr.org:1337&tr=udp:%2F%2Ftracker.zer0day.to:1337%2Fannounce&tr=udp:%2F%2Ftracker.leechers-paradise.org:6969%2Fannounce&tr=udp:%2F%2F9.rarbg.me:2850%2Fannounce&tr=udp:%2F%2F9.rarbg.to:2920%2Fannounce&tr=udp:%2F%2Fexplodie.org:6969%2Fannounce"
    )
    assert result["size"] == "3233284897"
    assert result["desc_link"] == "https://thepiratebay.org/description.php?id=18318043"
    assert result["epoch"] == "1502136130"
    assert type(result["seeds"]) == int
    assert type(result["leech"]) == int


def test_no_result():
    results = list(PiratebayEngine().search(r"ubuntu%2006.04"))
    assert results == []
