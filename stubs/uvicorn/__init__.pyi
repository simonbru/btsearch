from typing import Optional, Union
from wsgiref.types import WSGIApplication

def run(
    app: Union[WSGIApplication, str],
    *,
    host: Optional[str],
    port: Optional[int],
    reload: Optional[bool]
) -> None: ...
