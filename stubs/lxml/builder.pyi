from typing import Protocol

from lxml.etree import _Element


class PartialElementMakerCall(Protocol):
    """
    Represents type of e.g.
    `E.tag_name`
    equivalent to
    `functools.partial(E, "tag_name")`
    equivalent to
    `functools.partial(E.__call__, "tag_name")`
    """

    @staticmethod
    def __call__(*args: str | _Element, **kwargs: str) -> _Element: ...

class ElementMaker:
    def __call__(
        self, element_name: str, *args: str | _Element, **kwargs: str
    ) -> _Element: ...
    def __getattr__(self, name: str) -> PartialElementMakerCall: ...

E: ElementMaker
